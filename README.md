# KVM Controller

## Development

### Installation

1. Install VMware (either Fusion or Workstation)
1. Install Vagrant
1. Install vagrant-vmware-provider
1. Install Ansible
1. Install Unison (`brew install unison`)
1. Install Ansible Roles: `ansible-galaxy install -r requirements.yml`
1. Start the environment: `vagrant up`
1. Start Unison (`unison kvm-controller.prf`)
1. Create an ssh key pair for use in your Vagrant box:
    1. `ssh-keygen -t rsa -C <your_email_address>`
    1. When it asks where to save the key, enter `~/.ssh/vagrant/id_rsa`
    1. Add the public key to Github (so that NPM can update all dependencies)

## Troubleshooting

### Unison

#### The source file <paths> has been modified during synchronization

This sometimes happens when first syncing, to fix try running:

```sh
unison kvm-controller -force .
```
