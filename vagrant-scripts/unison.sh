#!/usr/bin/env bash

PROJECT_NAME="kvm-controller"

if [ -z ${USERPROFILE+x} ]; then
    HOME_DIR=$HOME
else
    HOME_DIR=$USERPROFILE
fi
UNISON_DIR=$HOME_DIR/.unison

# create ssh-config file
SSH_CONFIG_FILE="$UNISON_DIR/$PROJECT_NAME.ssh-config"
vagrant ssh-config > "$SSH_CONFIG_FILE"

# create unison profile
profile="
root = .
root = ssh://default//vagrant/

ignore = Name .DS_Store
ignore = Name ansible.cfg
ignore = Name requirements.yml
ignore = Name Vagrantfile
ignore = Path node_modules
ignore = Path ansible
ignore = Path .vscode
ignore = Path .vagrant
ignore = Path .git
ignore = Path vagrant-scripts

prefer = .
repeat = 2
terse = true
dontchmod = true
perms = 0
sshargs = -F $SSH_CONFIG_FILE
"

# write profile
cd $UNISON_DIR
[ -d .unison ] || mkdir .unison
echo "$profile" > "$UNISON_DIR/$PROJECT_NAME.prf"
