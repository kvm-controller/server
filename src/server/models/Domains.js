import libvirt from 'libvirt-node';
import fastXmlParser from 'fast-xml-parser';

import LibvirtClient from '../utils/LibvirtClient';
import parserOptions from '../utils/xml-parser-config';

const xmlParser = (domains) => {
    const domainsWithInfo = [];

    Array.from(domains).forEach(domain => {
        const xmlDesc = domain.getXMLDesc(0);
        const domainInfo = fastXmlParser.parse(xmlDesc, parserOptions).domain;
        domainsWithInfo.push(domainInfo);
    });

    return domainsWithInfo;
}

export default {
    all: () => {
        // https://libvirt.org/html/libvirt-libvirt-domain.html#virConnectListAllDomains
        const domains = LibvirtClient.listAllDomains(0);
        return xmlParser(domains);
    },
    getByUuid: (domainUuid) => {
        const domain = LibvirtClient.lookupByUUIDString(domainUuid);
        const xmlDesc = domain.getXMLDesc(0);
        const parsedXml = fastXmlParser.parse(xmlDesc, parserOptions);
        return parsedXml;
    },
    allCompact: () => {
        const domainsList = LibvirtClient.listAllDomains(0);
        const domains = [];

        domainsList.forEach(domain => {
            const domainInfo = {};
            domainInfo['name'] = domain.getName();
            domainInfo['uuid'] = domain.getUUIDString();
            domainInfo['id'] = domain.getID();
            domainInfo['osType'] = domain.getOSType();
            domainInfo['isActive'] = Boolean(domain.isActive());
            domainInfo['isPersistent'] = Boolean(domain.isPersistent());

            domains.push(domainInfo);
        });

        const quantity = domains.length || 0;

        return { quantity, domains }
    },
    active: () => {
        const domainsXml = LibvirtClient.listAllDomains(libvirt.VIR_CONNECT_LIST_DOMAINS_ACTIVE);
        const domains = xmlParser(domainsXml);

        const quantity = domains.length || 0;

        return { quantity, domains }
    },
    inactive: () => {
        const domainsXml = LibvirtClient.listAllDomains(libvirt.VIR_CONNECT_LIST_DOMAINS_INACTIVE);
        const domains = xmlParser(domainsXml);

        const quantity = domains.length || 0;

        return { quantity, domains }
    },
    create: (domainXml, volumeXml, storagePoolName = 'default') => {
        const result = LibvirtClient.defineXML(domainXml);
        // libvirt.storagePoolLookupByUUIDString(uuidstr);  // should probably use this eventually
        const storagePool = LibvirtClient.storagePoolLookupByName(storagePoolName);
        storagePool.createXML(volumeXml, 0);
        return result;
    },
    destroy: (domainUuid) => {
        if (!domainUuid) {
            return false;
        }

        const domain = LibvirtClient.lookupByUUIDString(domainUuid);
        const domainInfo = fastXmlParser.parse(domain.getXMLDesc(0), parserOptions).domain;
        const diskPath = domainInfo.devices.disk[0].source.attr['@_file'];
        const volume = LibvirtClient.storageVolLookupByPath(diskPath);

        let result = true;

        try {
            domain.undefine();
            domain.destroy();
            volume.wipe(0);
            volume.delete(0);
        } catch (err) {
            console.error(err);
            result = err;
        }

        return result;
    }
}
