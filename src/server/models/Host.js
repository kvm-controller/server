import LibvirtClient from '../utils/LibvirtClient';
import fastXmlParser from 'fast-xml-parser'; 

import parserOptions from '../utils/xml-parser-config';

export default {
    capabilities: () => {
        const capabilitiesXml = LibvirtClient.getCapabilities();
        return fastXmlParser.parse(capabilitiesXml, parserOptions)
    }
}
