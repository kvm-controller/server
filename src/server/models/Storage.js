import LibvirtClient from '../utils/LibvirtClient';

export default {
    all: () => {
        const storagePools = [];
        const listAllStoragePools = LibvirtClient.listAllStoragePools(0);
        
        listAllStoragePools.forEach(pool => {
            const currentStoragePool = {};
            currentStoragePool['name'] = pool.getName();
            currentStoragePool['uuid'] = pool.getUUIDString();
            currentStoragePool['isActive'] = pool.isActive();
            currentStoragePool['isPersistent'] = pool.isPersistent();
            storagePools.push(currentStoragePool);
        });

        const quantity = listAllStoragePools.length || 0;
        
        return { quantity, storagePools };
    },
    pool: (storagePoolName = 'default') => {
        const pool = LibvirtClient.storagePoolLookupByName(storagePoolName);
        const volumes = pool.listAllVolumes(0);
        const volumesInPool = [];

        volumes.forEach(volume => {
            const currentVolume = {};
            currentVolume['name'] = volume.getName();
            currentVolume['key'] = volume.getKey();
            currentVolume['path'] = volume.getPath();
            volumesInPool.push(currentVolume);
        });

        const quantity = pool.numOfVolumes();

        return { quantity, volumes: volumesInPool };
    },
}
