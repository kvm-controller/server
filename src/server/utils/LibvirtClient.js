import libvirt from 'libvirt-node/lib/index';

let connection;

try {
    connection = libvirt.open('qemu:///system');
} catch (err) {
    console.error(err);
}

export default connection;
