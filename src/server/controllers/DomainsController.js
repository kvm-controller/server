import fs from 'fs';

import Domains from '../models/Domains';

const domainXmlFile = fs.readFileSync('/vagrant/src/server/xml/domain.xml').toString();
const volumeXmlFile = fs.readFileSync('/vagrant/src/server/xml/volume.xml').toString();

export default {
    index: (req, res) => {
        switch (req.params.state) {
            case 'active':
                res.json(Domains.active());
                break;
            
            case 'inactive':
                res.json(Domains.inactive());
                break;
            
            default:
                res.json(Domains.allCompact());
                break;
        }
    },
    getByUuid: (req, res) => res.json(Domains.getByUuid(req.params.uuid)),
    create: (req, res) => {
        let domainXml;
        let volumeXml;

        if (req.body.domainXmlString) {
            domainXml = req.body.domainXmlString;
        } else {
            domainXml = domainXmlFile;
        }

        if (req.body.volumeXmlString) {
            volumeXml = req.body.volumeXmlString;
        } else {
            volumeXml = volumeXmlFile;
        }

        try {
            Domains.create(domainXml, volumeXml);
            res.json({ result: true });
        } catch (error) {
            console.info(error);  // eslint-disable-line no-console
            res.json({ result: false, error: error.toString() });
        }
    },
    destroy: (req, res) => res.json(Domains.destroy(req.params.id))
}
