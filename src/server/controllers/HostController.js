import Host from '../models/Host';

export default {
    index: (req, res) => res.json(Host.capabilities())
}
