import Storage from '../models/Storage';

export default {
    index: (req, res) => {
        res.json(Storage.all());
    },
    getPoolByName: (req, res) => {
        res.json(Storage.pool(req.params.name));
    },
    // getAllVolumesInPool: (req, res) => {
    //     res.json(Storage.pool(req.params.name));
    // }
}
