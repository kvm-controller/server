import http from 'http';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import errorHandler from 'errorhandler';
import helmet from 'helmet';

// import HomeController from './controllers/HomeController';
import DomainsController from './controllers/DomainsController';
import HostController from './controllers/HostController';
import StorageController from './controllers/StorageController';

const app = express();
app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use(helmet());
app.use(helmet.noCache());

app.get('/health', (req, res) => res.send(200));
app.get('/domain/:uuid', DomainsController.getByUuid);
app.get('/domains/:state?', DomainsController.index);
app.post('/domains', DomainsController.create);
// app.put('/domains', DomainsController.update);
app.delete('/domains/:id', DomainsController.destroy);
app.get('/host/capabilities', HostController.index);
app.get('/storage/pools', StorageController.index);
app.get('/storage/pool/:name', StorageController.getPoolByName);
// app.get('/', HomeController.index);

if (app.get('env') === 'development') {
    app.use(errorHandler());
}

const server = http.createServer(app);
server.listen(app.get('port'), () => {
    console.info('Express server listening on port ' + app.get('port'));  // eslint-disable-line no-console
});
