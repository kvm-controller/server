# Notes

## Phase 1 features

- [ ] List all domains
- [ ] List active domains only
- [ ] List inactive domains only
- [ ] Delete a domains

# References

- [Vagrant and Unison without a plugin](https://keylocation.sg/our-tech/vagrant-and-unison-without-a-plugin)
